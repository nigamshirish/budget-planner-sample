// Lib to create guid
const s4 = () =>
  Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1)

export const guid = () => s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4()

export const num = param => parseFloat(param) || 0.0

export const reducer = (items, field) =>
  items.reduce((acc, item) => {
    return parseInt(acc) + parseInt(item[field])
  }, 0)
